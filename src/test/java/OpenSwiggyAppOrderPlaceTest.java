import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class OpenSwiggyAppOrderPlaceTest {
    
    @Test
    public void openSwiggyApp() throws MalformedURLException, InterruptedException {

        File appDir = new File("src");
        File appFIle = new File(appDir, "Swiggy Food Order Delivery_v3.14.3_apkpure.com.apk");
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.DEVICE_NAME,"Samsung");
        cap.setCapability(MobileCapabilityType.APP, appFIle.getAbsolutePath());
        cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100");
        cap.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
        cap.setCapability("autoGrantPermissions", true);

        //cap.setCapability("appActivity","in.swiggy.android.activities.*");
        //cap.setCapability("appActivity","in.swiggy.android.activities.NewUserExperienceActivity");
        cap.setCapability("appActivity","in.swiggy.android.activities.HomeActivity");
        cap.setCapability("appPackage","in.swiggy.android");


        AndroidDriver<AndroidElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        Thread.sleep(3000);
        driver.findElementById("in.swiggy.android:id/login_layout").click();
        Thread.sleep(3000);
        driver.findElementByClassName("android.widget.EditText").click();
        Thread.sleep(3000);
        // You can give ur mobile number and OTP Manually because i will get otp.
        //driver.getKeyboard().sendKeys("9663656177");
        Thread.sleep(5000);
        driver.findElementById("in.swiggy.android:id/loginCheckButton").click();
        Thread.sleep(7000);
        //Please Enter OTP Manually
        driver.findElementById("in.swiggy.android:id/forgotPasswordSubmitBtn").click();
        driver.findElementByXPath("//android.widget.TextView[@text='VERIFY AND PROCEED'").click();

        //close  swigyy go
        driver.findElementById("in.swiggy.android:id/buttonLayout").click();

        driver.findElementByXPath("//android.widget.TextView[@text='Offers Near You']").click();

        Thread.sleep(5000);

        // click on 1st image section
        driver.findElementByXPath("//android.widget.ImageView[@index=1]").click();

        //Adding to the cart
        driver.findElementByXPath("(//android.widget.RelativeLayout[@content-desc='Add Item'])[1]").click();

        // click on view cart
        driver.findElementByXPath("//android.widget.ImageView[@index=1]").click();

        // click on ok popup button
        driver.findElementByXPath("//android.widget.TextView[@text='OK']").click();

        // Click on Proceed to pay
        driver.findElementByXPath("//android.widget.TextView[@text='PROCEED TO PAY']").click();

        Thread.sleep(3000);

        //Scroll the page and click on Cash
        MobileElement el = (MobileElement) driver
                .findElementByAndroidUIAutomator("new UiScrollable("
                        + "new UiSelector().scrollable(true)).scrollIntoView("
                        + "new UiSelector().textContains('Please keep exact change handy to help us serve you better'));");
        el.click();

        //Click on Pay With Cash
        driver.findElementByXPath("//android.view.ViewGroup[@content-desc='PAY WITH CASH']").click();

    }
    
}
